from django.conf.urls import include,url
from .views import Books, Search, Authors

urlpatterns = [

    url(r'books', Books.as_view(), name='books'),
    url(r'authors', Authors.as_view(), name='authors'),
    url(r'search/', Search.as_view(), name='search')
]