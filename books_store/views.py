import logging
from django.shortcuts import render
from django.views.generic import View
from django.http import JsonResponse
from django.http import HttpResponseBadRequest
from .Utils import *
import logging
log = logging.getLogger(__name__)
# Create your views here.

class Search(View):

    def get(self,request):

        '''
            Method to search for the input query parameters and return the result
            Pseudo-code:
                Search term, result type and top parameters are fetched from input request
                Search Mapping method is called with the search term and result type to get the appropriate search method
                Then Appropriate Search Operation method is called to get the results
                If result exists - return the json response of result, else return custom message
                If any exception arises - return the appropriate exception to the client

        :param request: Input Search Parameter requests
        : arg1 : search_term - search term to search by (authors, books etc)
        : arg2 : result_type - result type ( successfull, unsuccessfull etc)
        : arg3 : top - top search resutls (top 5, top 10 etc)
        :return: Search Results or custom message
        : rtype: Json / Exception
        : raises : HttpResponseBadRequest / Exception
        '''

        try:
            search_term = request.GET.get('search_term')
            result_type = request.GET.get('result_type')
            top = request.GET.get('top')
            search_method = search_mapping(search_term,result_type)
            result = search_method(top)
            if not result:
                return JsonResponse({"Message" : "No Available Results for the input query parameters"})

            return JsonResponse(result,safe=False)

        except NoAvaiableSearchMethod as nse:
            log.error("No Available Search Method for the input query parameters")
            return HttpResponseBadRequest("No Available Search for the input query parameters")

        except Exception as e:
            log.error("Exception in Search API Get Method : %s", e)
            raise e

class Books(View):

    def get(self):
        pass

    def post(self):
        pass

class Authors(View):

    def get(self):
        pass

    def post(self):
        pass