from __future__ import unicode_literals

from django.apps import AppConfig


class BooksStoreConfig(AppConfig):
    name = 'books_store'
