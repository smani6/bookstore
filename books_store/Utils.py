from .models import books, books_inventory, publishers,customers,authors
from django.db.models import Count
from .Exceptions import NoAvaiableSearchMethod

def search_mapping(search_term,result_type):

    '''
        Mapping method to return the exact search method
        for the input search_term and result type

    :param search_term: Input search_term (authors, books etc)
    :param result_type: Input result_type ( successfull, unsuccessfull etc)
    :return: Search Method
    '''
    mapping = {
        'authors':{
            'successfull': search_successfull_author_with_books,
            'unsuccessfull': search_unsuccessfull_author_with_books,
            'no_books': search_author_with_no_books,

        }

    }
    search_method = mapping[search_term].get(result_type)

    if not search_method:
        raise NoAvaiableSearchMethod

    return search_method

def search_successfull_author_with_books(filter):

    '''
        Method to search and return successfull authors with their books
        Filtered by the input filter parameter

    :param filter: top results (top 5, top 10 etc)
    :return: Successfull authors with their books
    '''
    best_selling_books = books_inventory.objects.annotate(Count('books_sold')).order_by('-books_sold')[:filter]
    result = []
    for best_selling_book in best_selling_books:
        details = {}
        book_details = books.objects.get(book_id=best_selling_book.id)
        authors = book_details.authors.all()
        for author in authors:
            if any(author.name in d['author_name'] for d in result):
                for d in result:
                    if d['author_name'] == author.name:
                        d['books'].append(get_book_details(book_details))
            else:
                details.update({'author_name' : author.name})
                details.update({'books': [get_book_details(book_details)] })
                result.append(details)

    return result

def get_book_details(book_details):

    '''
        Method to return the book_dict from the input book_details objects

    :param book_details: book_details object
    :return: Book dict containing book details
    '''
    return {'name' : book_details.name, 'isbn':book_details.isbn,
            'type': book_details.type, 'year_of_publication': book_details.year_of_publication,
            'price': book_details.price}

def search_unsuccessfull_author_with_books():
    pass

def search_author_with_no_books():
    pass

def search_best_books(filters):
    pass