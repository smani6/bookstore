from __future__ import unicode_literals

from django.db import models

class books_inventory(models.Model):

    '''
        Books Inventory Data Model
    '''
    id = models.AutoField(primary_key=True)
    available_books = models.IntegerField()
    books_sold = models.IntegerField(null=True)

    def __unicode__(self):
        return ( "%s, %s, %s" %(self.id, self.available_books, self.books_sold))

class authors(models.Model):
    '''
        Authors Data Model
    '''
    author_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=4098)

    def __unicode__(self):
        return ("%s, %s" % (self.author_id, self.name))

class publishers(models.Model):
    '''
        Publishers Data Model
    '''

    publisher_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=4098)
    email_id = models.CharField(max_length=4098, null=True)
    mobile = models.CharField(max_length=20, null=True)

    def __unicode__(self):
        return ("%s, %s, %s" % (self.publisher_id, self.book_id, self.name))

class books(models.Model):
    '''
        Books Data Model
    '''
    book_id = models.ForeignKey(books_inventory,unique=True)
    authors = models.ManyToManyField(authors)
    publisher_id = models.ForeignKey(publishers)
    isbn = models.BigIntegerField()
    name = models.CharField(max_length=4098)
    type = models.CharField(max_length=1024, null=True)
    year_of_publication = models.IntegerField()
    price = models.DecimalField(max_digits=32, decimal_places=5, null=True)

    def __unicode__(self):
        return ("%s, %s" %(self.book_id,self.name))

class customers(models.Model):
    '''
        Customers Data Model
    '''

    customer_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=4098)
    last_name = models.CharField(max_length=4098)
    email_id = models.CharField(max_length=4098)
    mobile = models.CharField(max_length=4098,null=True)
    street_name = models.CharField(max_length=4098,null=True)
    postal_code = models.CharField(max_length=10,null=True)
    country = models.CharField(max_length=100,null=True)

    def __unicode__(self):
        return ("%s, %s, %s" % (self.customer_id, self.first_name, self.last_name))

class orders(models.Model):
    '''
        Orders Data Model
    '''

    order_id = models.AutoField(primary_key=True)
    customer_id = models.ForeignKey(customers)
    date = models.DateTimeField(auto_now_add=True)
    shipping_address = models.CharField(max_length=10248)
    total_amount = models.DecimalField(max_digits=32, decimal_places=5)

    def __unicode__(self):
        return ("%s,%s" % (self.order_id, self.customer_id))

class order_details(models.Model):
    '''
        Order Details Data Model
    '''
    id = models.AutoField(primary_key=True)
    order_id = models.ForeignKey(orders)
    book_id = models.ForeignKey(books)
    quantity = models.IntegerField()

    def __unicode__(self):
        return ("%s,%s,%s,%s" % (self.id, self.order_id,self.book_id, self.quantity))
