Application: Online BookStore. API designed to search and retrieve successfull author and their books 

Application hosted at Openshift : http://bookstore-githubusersdata.rhcloud.com/search?search_term=authors&result_type=successfull&top=2

Services are based on REST API and details retrieved as below 


```
#!python

URL : http://bookstore-githubusersdata.rhcloud.com/search?search_term=authors&result_type=successfull&top=2

Response:

[
  {
    "books": [
      {
        "type": null,
        "price": "40.00000",
        "isbn": 4444,
        "name": "book4",
        "year_of_publication": 2016
      },
      {
        "type": null,
        "price": "30.00000",
        "isbn": 3333,
        "name": "book3",
        "year_of_publication": 2016
      }
    ],
    "author_name": "Leo Tolstoy"
  },
  {
    "books": [
      {
        "type": null,
        "price": "30.00000",
        "isbn": 3333,
        "name": "book3",
        "year_of_publication": 2016
      }
    ],
    "author_name": "Chetan Bhagat"
  }
]

```
